const { assert } = require("chai");

const Ownership = artifacts.require("./Ownership.sol");

require("chai").use(require("chai-as-promised")).should();

contract("Ownership", (accounts) => {
  let contract;

  before(async () => {
    contract = await Ownership.deployed();
  });

  describe("deployment", async () => {
    it("deploys successfully", async () => {
      const address = contract.address;
      assert.notEqual(address, 0x0);
      assert.notEqual(address, "");
      assert.notEqual(address, null);
      assert.notEqual(address, undefined);
    });
  });

  describe("Monster", async () => {
    describe("creation", async () => {
      it("creates new monster", async () => {
        const result = await contract.create("Monster 1", 0, {
          from: accounts[1],
          value: 10000000000000000, // 0.01 ether
        });
        assert.equal(
          result.logs[0].args._id,
          0,
          "Should have 1 monster in the list"
        );
        ownerMonster = await contract.monsters(0);
        assert.equal(ownerMonster.name, "Monster 1", "Name does not match");
        assert.equal(ownerMonster.element, 0, "Element does not match");
        assert.equal(ownerMonster.level, 1, "Current level should be 1");
        assert.equal(ownerMonster.hp, 10, "HP is 10");
        assert.equal(ownerMonster.atk, 2, "Attakc is 2");
        assert.equal(ownerMonster.def, 2, "Defense is 2");
        const owner = await contract.monsterToOwner(0);
        assert.equal(owner, accounts[1], "Owner does not match");
      });

      it("cannot create new monster with same name", async () => {
        await contract.create("Monster 1", 0, {
          from: accounts[1],
          value: 10000000000000000, // 0.01 ether
        }).should.be.rejected;
      });

      it("creates another monster", async () => {
        const result = await contract.create("Monster 2", 1, {
          from: accounts[2],
          value: 10000000000000000, // 0.01 ether
        });
        assert.equal(
          result.logs[0].args._id,
          1,
          "Should have 2 monster in the list"
        );
        opponentMonster = await contract.monsters(1);
        assert.equal(opponentMonster.name, "Monster 2", "Name does not match");
        assert.equal(opponentMonster.element, 1, "Element does not match");
        assert.equal(opponentMonster.level, 1, "Current level should be 1");
        const owner = await contract.monsterToOwner(1);
        assert.equal(owner, accounts[2], "Owner does not match");
      });
    });

    let eBattle;

    describe("battle", async () => {
      it("cannot battle self", async () => {
        await contract.battle(0, 0, {
          from: accounts[1],
        }).should.be.rejected;
      });

      it("has a battle", async () => {
        const result = await contract.battle(0, 1, {
          from: accounts[1],
        });

        eBattle = result.logs[1].args;
        assert.equal(eBattle._dmg, 15);
        assert.equal(eBattle._win, true);
      });

      it("cools down after battle", async () => {
        await contract.battle(0, 0, {
          from: accounts[1],
        }).should.be.rejected;
      });
    });

    describe("level & status", async () => {
      it("levels up", async () => {
        ownerMonster = await contract.monsters(0);
        assert.equal(ownerMonster.level, 2, "Current level should be 2");
        assert.equal(ownerMonster.points, 3, "Should get 3 points");
      });

      it("raises its attack", async () => {
        await contract.raiseAttack(0, {
          from: accounts[1],
        });
        ownerMonster = await contract.monsters(0);
        assert.equal(ownerMonster.atk, 3, "Attack is 3");
      });

      it("raises its defense", async () => {
        await contract.raiseDefense(0, {
          from: accounts[1],
        });
        ownerMonster = await contract.monsters(0);
        assert.equal(ownerMonster.atk, 3, "Defense is 3");
      });

      it("raises its HP", async () => {
        await contract.raiseHealth(0, {
          from: accounts[1],
        });
        ownerMonster = await contract.monsters(0);
        assert.equal(ownerMonster.hp, 20, "HP is 20");
      });
    });
  });

  describe("Monster Ownership", async () => {
    it("has balance (monster count)", async () => {
        const count = await contract.balanceOf(accounts[1]);
        assert.equal(count, 1, "1 Monster");
      });

      it("has owner", async () => {
        const owner = await contract.ownerOf(0);
        assert.equal(owner, accounts[1], "Owner is account 1");
      });

      it("transfers monster to new owner", async () => {
        const result = await contract.transferFrom(accounts[1], accounts[2], 0, {
            from: accounts[1]
        });
        const senderCount = await contract.balanceOf(accounts[1]);
        assert.equal(senderCount, 0, "Expected 0 monster");
        const recipientCount = await contract.balanceOf(accounts[2]);
        assert.equal(recipientCount, 2, "Expected 2 monsters");
      });

  });
});
