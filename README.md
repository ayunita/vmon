# VMON - Virtual Monster DApp
Create a NFT virtual monster that can fight and be traded.

### Link to demo: [https://vmon.netlify.app/](https://vmon.netlify.app/)

## Design
![Diagram](dapp-diagram.png)

## Installation
Use npm to install all dependencies
```console
npm install
```

### Smart Contract
Use [truffle](https://www.trufflesuite.com/docs/truffle/getting-started/installation) to test, compile, and deploy the smart contract.

Launch development network
```console
truffle develop
```

Test the smart contract
```console
truffle test
```

#### Localhost
Deploy to localhost network
```console
truffle deploy
```

#### Rinkeby Testnet
Populate .env file
```console
PRIVATE_KEY=<Key>
HTTPS_URI=<Rinkeby HTTPS URI>
WS_URI=<Rinkeby Websocket URI>
CONTRACT_ADDRESS=0x10628336Ea2FD48e0E512d41D78ce10dee184Bb5
```

Deploy to rinkeby testnet
```console
truffle deploy --network rinkeby
```

### Client
Run the app
```console
npm start
```

## Future Fix:
- Damage formula is unbalanced

## Future Development:
- Users will be able to draw their own monster.
- The image will be stored in IPFS, and the hash will be captured by the smart contract.
- Users will be able to trade
- Users will be able to sell and buy monsters

## License
MIT