import airMonster from "../images/air-monster.png";
import waterMonster from "../images/water-monster.png";
import fireMonster from "../images/fire-monster.png";
import earthMonster from "../images/earth-monster.png";

export const MONSTERPEDIA = {
  0: {
    name: "Air",
    icon: "💨",
    src: airMonster,
    weak: "Earth",
    strong: "Water",
  },
  1: {
    name: "Water",
    icon: "💧",
    src: waterMonster,
    weak: "Air",
    strong: "Fire",
  },
  2: {
    name: "Fire",
    icon: "🔥",
    src: fireMonster,
    weak: "Water",
    strong: "Earth",
  },
  3: {
    name: "Earth",
    icon: "🍃",
    src: earthMonster,
    weak: "Fire",
    strong: "Air",
  },
};
