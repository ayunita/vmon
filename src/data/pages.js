import Generator from "../components/Generator";
import Explore from "../components/Explore";
import Collection from "../components/Collection";
import Home from "../components/Home";

export const PAGES = ({
    0: {
        title: "Home",
        path: "/home",
        child: <Home />,
      },
  1: {
    title: "Create",
    path: "/create",
    child: <Generator />,
  },
  2: {
    title: "Collection",
    path: "/collection",
    child: <Collection />,
  },
  3: {
    title: "Explore",
    path: "/link",
    child: <Explore />,
  },
});
