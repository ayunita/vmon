import React, { useState, useEffect, useContext } from "react";
import { Row, Col, Form, Input, Radio, Button, Spin, notification } from "antd";
import { CheckCircleTwoTone, CloseCircleTwoTone } from "@ant-design/icons";
import { ContractContext } from "../context/ContractContext";
import { MONSTERPEDIA } from "../data/monsterpedia";

const Generator = () => {
  const contract = useContext(ContractContext);

  const [form] = Form.useForm();
  const [ownership, setOwnership] = useState();
  const [account, setAccount] = useState();
  const [image, setImage] = useState({
    ...MONSTERPEDIA["0"],
  });
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    const { instance, account } = contract;
    setOwnership(instance);
    setAccount(account);
  }, [contract]);

  /**
   * Display notification
   * @param {string} title
   * @param {string} description
   * @param {ReactNode} icon
   */
  const openNotification = (title, description, icon) => {
    notification.open({
      message: title,
      description: description,
      icon: icon,
    });
  };

  /**
   * Set monster image when element was changed
   * @param {event} e
   */
  const handleElementChange = (e) => {
    setImage({ ...MONSTERPEDIA[`${e.target.value}`] });
  };

  /**
   * Create a new monster
   * @param {object} values
   */
  const handleSubmit = async (values) => {
    const { name, element } = values;
    setLoading(true); // Loading
    // Create monster
    try {
      let result = await ownership.methods.create(name, Number(element)).send({
        from: account,
        gas: 1500000,
        value: 10000000000000000,
        gasPrice: "30000000000",
      });
      if (result) {
        if (result.status)
          openNotification(
            "New Monster Created!",
            `Groarrr! ${name} is ready to explore!`,
            <CheckCircleTwoTone twoToneColor="#228B22" />
          );
      }
    } catch (error) {
      if (error.code === 4001) {
        openNotification(
          "Oops!",
          `Transaction is cancelled.`,
          <CloseCircleTwoTone twoToneColor="#ff0000" />
        );
      } else {
        openNotification(
          "Oops!",
          `It seems the name is taken.`,
          <CloseCircleTwoTone twoToneColor="#ff0000" />
        );
      }

      // console.log("Generator: ", error);
    }
    setLoading(false); // Done
  };

  return (
    <>
      <Spin tip="Your monster is on the way..." spinning={loading}>
        <Row
          type="flex"
          style={{ alignItems: "center", marginTop: 10 }}
          justify="center"
        >
          <Col>
            <img className="image-fit" src={image.src} alt="monster" />
          </Col>
          <Col>
            <Form
              form={form}
              initialValues={{ name: "", element: "0" }}
              onFinish={handleSubmit}
              layout="vertical"
            >
              <Form.Item
                label="Monster Name"
                name="name"
                rules={[
                  { required: true, message: "Your monster needs name!" },
                ]}
              >
                <Input size="large" placeholder="Sherbey" />
              </Form.Item>
              <Form.Item label="Element" required name="element">
                <Radio.Group
                  size="large"
                  options={Object.keys(MONSTERPEDIA).map((i) => ({
                    label: `${MONSTERPEDIA[i].name} ${MONSTERPEDIA[i].icon}`,
                    value: i,
                  }))}
                  optionType="button"
                  buttonStyle="solid"
                  onChange={handleElementChange}
                />
              </Form.Item>
              <Form.Item>
                <Col style={{ fontSize: "16px" }}>
                  <Row style={{ color: "green" }}>
                    Effective against {image.strong}
                  </Row>
                  <Row style={{ color: "red" }}>Weak against {image.weak}</Row>
                </Col>
              </Form.Item>
              <Form.Item>
                <Button size="large" type="primary" htmlType="submit">
                  Create
                </Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </Spin>
    </>
  );
};

export default Generator;
