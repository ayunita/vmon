import React, { useState, useEffect, useContext } from "react";
import { Row, Col, Spin, Button, List, Avatar } from "antd";
import { ContractContext } from "../context/ContractContext";
import { MONSTERPEDIA } from "../data/monsterpedia";
import Status from "./Status";
import Battle from "./Battle";

const Collection = () => {
  const contract = useContext(ContractContext);
  const [ownership, setOwnership] = useState();
  const [account, setAccount] = useState();
  const [loading, setLoading] = useState(false);
  const [monsters, setMonsters] = useState([]);
  const [selectedMonster, setSelectedMonster] = useState(0);
  const [battle, setBattle] = useState(false);

  useEffect(() => {
    const { instance, account } = contract;
    setOwnership(instance);
    setAccount(account);

    // Retrieve owner's monsters
    if (ownership) {
      loadMyMonsters();
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [contract, ownership]);

  /**
   * Get all monsters owned by logged-in account
   */
  const loadMyMonsters = async () => {
    setLoading(true); // Loading
    let result = await ownership.methods.getMonsterIdsByOwner(account).call();

    if (result) {
      let monsterList = [];
      // Get monsters that belong to this account
      for (const i in result) {
        let mon = await ownership.methods.monsters(result[i]).call();
        monsterList.push({ ...mon, monsterId: result[i] });
      }
      setMonsters(monsterList);
    }
    setLoading(false); // List is loaded
  };

  return (
    <div>
      <Spin tip="Loading..." spinning={loading}>
        <h1>Collection</h1>
        <div className="collection">
          <div className="collection-col">
            <List
              itemLayout="horizontal"
              dataSource={monsters}
              renderItem={(mon, index) => {
                const nextBattle =
                  mon.battleTime - Math.floor(+new Date() / 1000);
                return (
                  <List.Item
                    style={{
                      background:
                        selectedMonster === index ? "#f5f5f5" : "transparent",
                    }}
                    actions={[
                      <Button
                        type="text"
                        onClick={() => {
                          setSelectedMonster(index);
                          setBattle(false);
                        }}
                      >
                        Status
                      </Button>,
                      <Button
                        disabled={nextBattle > 0}
                        type="text"
                        onClick={() => {
                          setSelectedMonster(index);
                          setBattle(true);
                        }}
                      >
                        Battle
                      </Button>,
                    ]}
                  >
                    <List.Item.Meta
                      avatar={
                        <Avatar src={MONSTERPEDIA[`${mon.element}`].src} />
                      }
                      title={mon.name}
                      description={
                        <>
                          <Row className="col-margin">
                            <Col>Level: {mon.level}</Col>
                            <Col>Win: {mon.win}</Col>
                            <Col>Lose: {mon.lose}</Col>
                          </Row>
                          <Row>
                            <Col>
                              Next battle:{" "}
                              {nextBattle < 0 ? (
                                <span style={{ color: "green" }}>now</span>
                              ) : (
                                <span style={{ color: "red" }}>
                                  {Math.ceil(nextBattle / 60)} minutes
                                </span>
                              )}
                            </Col>
                          </Row>
                        </>
                      }
                    />
                  </List.Item>
                );
              }}
            />
          </div>
          <div style={{ flex: 1 }}></div>
          <div className="collection-col">
            {monsters.length > 0 && !battle && selectedMonster !== null && (
              <Status
                monsterId={monsters[selectedMonster].monsterId}
                setLoading={setLoading}
              />
            )}
            {monsters.length > 0 && battle && selectedMonster !== null && (
              <Battle
              monsterLevel={monsters[selectedMonster].level}
                myMonsters={monsters}
                monsterId={monsters[selectedMonster].monsterId}
                setLoading={setLoading}
                loadMyMonsters={loadMyMonsters}
              />
            )}
          </div>
        </div>
      </Spin>
    </div>
  );
};

export default Collection;
