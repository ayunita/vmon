import React, { useState, useEffect, useContext } from "react";
import { Row, Col, Button, List, Avatar, notification } from "antd";
import { CloseCircleTwoTone } from "@ant-design/icons";
import { ContractContext } from "../context/ContractContext";
import { MONSTERPEDIA } from "../data/monsterpedia";

const Battle = ({
  myMonsters,
  monsterLevel,
  monsterId,
  setLoading,
  loadMyMonsters,
}) => {
  const contract = useContext(ContractContext);
  const [ownership, setOwnership] = useState();
  const [account, setAccount] = useState();
  const [opponents, setOpponents] = useState([]);

  useEffect(() => {
    const { instance, account } = contract;
    setOwnership(instance);
    setAccount(account);

    // Retrieve all monsters except owner's monsters
    loadMonsterList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [contract, myMonsters, ownership]);

  /**
   * Get all monsters except owner's monsters
   */
  const loadMonsterList = async () => {
    setLoading(true); // Loading
    if (ownership) {
      let result = await ownership.methods.getAllMonsters().call();
      if (result) {
        let my = [...myMonsters].map((m) => m.name);
        // Get opponents' monsters only, and restructure the object
        let opp = [];
        
        result.forEach((m, index) => {
          if (!my.includes(m.name))
            if (Number(m.level)-3 <= +monsterLevel && +monsterLevel <= Number(m.level)+3) {
              // Filter opponent level (+- 3 monster level)
              opp.push({
                id: index,
                name: m.name,
                level: m.level,
                element: m.element,
                win: m.win,
                lose: m.lose,
              });
            }
        });
        // Set list of available opponents
        setOpponents(opp);
      }
    }
    setLoading(false); // Done
  };

  /**
   * Display notification
   * @param {string} title
   * @param {string} description
   * @param {ReactNode} icon
   */
  const openNotification = (title, description, icon) => {
    notification.open({
      message: title,
      description: description,
      icon: icon,
    });
  };

  /**
   * Attack other monster with opponentId
   * @param {number} opponentId
   */
  const battleMonster = async (opponentId) => {
    setLoading(true); // Loading
    try {
      await ownership.methods.battle(monsterId, opponentId).send({
        from: account,
        gas: 150000,
        gasPrice: "300000000",
      });
      loadMyMonsters(); // Update UI
    } catch (error) {
      if (error.code === 4001) {
        openNotification(
          "Oops!",
          `Transaction is cancelled.`,
          <CloseCircleTwoTone twoToneColor="#ff0000" />
        );
      } else {
        openNotification(
          "Oops!",
          `Something went wrong.`,
          <CloseCircleTwoTone twoToneColor="#ff0000" />
        );
      }
      // console.log("Battle: ", error);
    }
    setLoading(false); // Done
  };

  return (
    <List
      itemLayout="horizontal"
      dataSource={opponents}
      renderItem={(mon) => (
        <List.Item
          actions={[
            <Button type="text" onClick={() => battleMonster(mon.id)}>
              Attack
            </Button>,
          ]}
        >
          <List.Item.Meta
            avatar={<Avatar src={MONSTERPEDIA[`${mon.element}`].src} />}
            title={mon.name}
            description={
              <>
                <Row className="col-margin">
                  <Col>
                    <div
                      className={`element-tag ${MONSTERPEDIA[
                        `${mon.element}`
                      ].name.toLowerCase()}`}
                    >
                      <span role="img" aria-label="monster element">
                        {MONSTERPEDIA[`${mon.element}`].icon}
                      </span>
                    </div>
                  </Col>
                  <Col>Level: {mon.level}</Col>
                  <Col>Win: {mon.win}</Col>
                  <Col>Lose: {mon.lose}</Col>
                </Row>
              </>
            }
          />
        </List.Item>
      )}
    />
  );
};

export default Battle;
