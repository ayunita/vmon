import React, { useState, useEffect, useContext } from "react";
import { Row, Col, Button, notification, Tooltip } from "antd";
import { CloseCircleTwoTone } from "@ant-design/icons";
import { ContractContext } from "../context/ContractContext";
import { MONSTERPEDIA } from "../data/monsterpedia";
import { PlusOutlined } from "@ant-design/icons";

const Status = ({ monsterId, setLoading }) => {
  const contract = useContext(ContractContext);
  const [ownership, setOwnership] = useState();
  const [account, setAccount] = useState();
  const [myMonster, setMyMonster] = useState();

  useEffect(() => {
    const { instance, account } = contract;
    setOwnership(instance);
    setAccount(account);
    // Update status UI
    getMyMonster(monsterId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [contract, ownership, monsterId]);

  /**
   * Get monster by monster ID
   * @param {number} monsterId 
   */
  const getMyMonster = async (monsterId) => {
    if (ownership) {
      let result = await ownership.methods.monsters(monsterId).call();
      setMyMonster({ ...result, monsterId: monsterId });
    }
  };

  /**
   * Increase monster status: hp/atk/def using point
   * @param {string} status
   * @param {number} monsterId
   */
  const raiseStatus = async (status, monsterId) => {
    try {
      setLoading(true); // Loading
      let result = null;
      if (status === "hp") {
        result = await ownership.methods.raiseHealth(monsterId).send({
          from: account,
        });
      } else if (status === "atk") {
        result = await ownership.methods.raiseAttack(monsterId).send({
          from: account,
        });
      } else if (status === "def") {
        result = await ownership.methods.raiseDefense(monsterId).send({
          from: account,
        });
      }
      if (result) {
        getMyMonster(monsterId); // Update status UI
      }
      //console.log("Status: ", result);
    } catch (error) {
      if (error.code === 4001) {
        openNotification(
          "Oops!",
          `Transaction is cancelled.`,
          <CloseCircleTwoTone twoToneColor="#ff0000" />
        );
      } else {
        openNotification(
          "Oops!",
          `Something went wrong.`,
          <CloseCircleTwoTone twoToneColor="#ff0000" />
        );
      }
    } finally {
      setLoading(false); // Done
    }
  };

  /**
   * Display notification
   * @param {string} title
   * @param {string} description
   * @param {ReactNode} icon
   */
  const openNotification = (title, description, icon) => {
    notification.open({
      message: title,
      description: description,
      icon: icon,
    });
  };

  return (
    <>
      {myMonster ? (
        <Row align="middle">
          <Col>
            <img
              className="image-fit"
              alt="monster"
              src={MONSTERPEDIA[`${myMonster.element}`].src}
            />
          </Col>
          <Col>
            <Col>
              <Row>MONSTER ID</Row>
              <Row className="mon-status">#{myMonster.monsterId}</Row>
              <Row>NAME</Row>
              <Row className="mon-status">{myMonster.name}</Row>
              <Row>ELEMENT</Row>
              <Row className="mon-status">
                {MONSTERPEDIA[`${myMonster.element}`].name}
              </Row>
              <Row>
                <Col span={12}>LEVEL</Col>
                <Col span={12}>
                  <Tooltip title="Every level up will receive 3 points.">
                    POINTS
                  </Tooltip>
                </Col>
              </Row>
              <Row className="mon-status">
                <Col span={12}>{myMonster.level}</Col>
                <Col span={12}>{myMonster.points}</Col>
              </Row>
              <table className="mon-status">
                <tbody>
                  <tr>
                    <td>
                      <span
                        img="role"
                        aria-label="health"
                        style={{ marginRight: "8px" }}
                      >
                        ❤️
                      </span>
                    </td>
                    <td>{myMonster.hp}</td>
                    <td>
                      <Button
                        disabled={Number(myMonster.points) === 0}
                        style={{ margin: 8 }}
                        size="small"
                        type="primary"
                        shape="circle"
                        icon={<PlusOutlined />}
                        onClick={() => raiseStatus("hp", myMonster.monsterId)}
                      />
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span
                        img="role"
                        aria-label="attack"
                        style={{ marginRight: "8px" }}
                      >
                        ⚔️
                      </span>
                    </td>
                    <td>{myMonster.atk}</td>
                    <td>
                      <Button
                        disabled={Number(myMonster.points) === 0}
                        style={{ margin: 8 }}
                        size="small"
                        type="primary"
                        shape="circle"
                        icon={<PlusOutlined />}
                        onClick={() => raiseStatus("atk", myMonster.monsterId)}
                      />
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span
                        img="role"
                        aria-label="defense"
                        style={{ marginRight: "8px" }}
                      >
                        🛡
                      </span>
                    </td>
                    <td>{myMonster.def}</td>
                    <td>
                      <Button
                        disabled={Number(myMonster.points) === 0}
                        style={{ margin: 8 }}
                        size="small"
                        type="primary"
                        shape="circle"
                        icon={<PlusOutlined />}
                        onClick={() => raiseStatus("def", myMonster.monsterId)}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </Col>
          </Col>
        </Row>
      ) : null}
    </>
  );
};

export default Status;
