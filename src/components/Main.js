import React, { useState, useEffect, useContext } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";
import { notification } from "antd";
import { BellTwoTone } from "@ant-design/icons";
import getWeb3 from "../services/getWeb3";
import OwnershipContract from "../abis/Ownership.json";
import { ContractActionsContext } from "../context/ContractContext";
import { PAGES } from "../data/pages.js";
import { Layout, Menu } from "antd";
const { Header, Content, Footer } = Layout;

const Main = (props) => {
  const setContract = useContext(ContractActionsContext);

  const [page, setPage] = useState(0);
  useEffect(() => {
    const loadContract = async () => {
      try {
        // Get network provider and web3 instance.
        const web3 = await getWeb3();

        // Use web3 to get the user's accounts.
        const accounts = await window.ethereum.request({
          method: "eth_accounts",
        });

        // Get the contract instance.
        const networkId = await web3.eth.net.getId();
        const deployedNetwork = OwnershipContract.networks[networkId];
        const instance = new web3.eth.Contract(
          OwnershipContract.abi,
          deployedNetwork && deployedNetwork.address
        );

        // Store contract instance to the context
        setContract({ web3: web3, account: accounts[0], instance: instance });

        // Listen to Battle event
        instance.events.Battle().on("data", async function (event) {
          console.log("Battle event:", event);
          const attacker = event.returnValues._name;
          const defender = event.returnValues._opponent;
          const attackerWin = event.returnValues._win;
          notification.open({
            message: "A Monster Battle just happened!",
            description: `${attacker} has ${
              attackerWin ? "defeated" : "lost against"
            } ${defender}`,
            icon: <BellTwoTone style={{ color: "#0000ff" }} />,
          });
        });
      } catch (e) {
        alert("Failed to load web3, accounts, or contract.");
        console.log(e);
      }
    };
    loadContract();
  }, [setContract]);

  return (
    <Layout className="layout">
      <Router>
        <Header>
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={[page]}
            style={{ float: "right" }}
            onSelect={(e) => setPage(e.key)}
          >
            {Object.keys(PAGES).map((i) => (
              <Menu.Item key={i}>
                <Link to={PAGES[i].path}>{PAGES[i].title}</Link>
              </Menu.Item>
            ))}
          </Menu>
        </Header>
        <Content>
          <div className="content">
            <Switch>
              <Route exact path="/">
                <Redirect to="/home" />
              </Route>
              {Object.keys(PAGES).map((i) => (
                <Route path={PAGES[i].path} key={i}>
                  {PAGES[i].child}
                </Route>
              ))}
            </Switch>
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Virtual Monster DApp ©{new Date().getFullYear()} | Image from{" "}
          <a
            target="_blank"
            href="https://www.freepik.com/"
            rel="noreferrer noopener"
          >
            freepik
          </a>
        </Footer>
      </Router>
    </Layout>
  );
};

export default Main;
