import React from "react";
import Main from "../components/Main";
import ContractProvider from "../context/ContractContext";
import "./App.scss";

const App = () => {
  return (
    <ContractProvider>
      <Main />
    </ContractProvider>
  );
};

export default App;
