import React from "react";

const Home = () => {
    return(
        <div>
            <h1>Welcome to Decentralized Virtual Monster.</h1>
            <h2>You need metamask to use this app.</h2>
        </div>
    )
}

export default Home;