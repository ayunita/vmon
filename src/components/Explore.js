import React, { useState, useEffect, useContext } from "react";
import { Row, Col, Spin, Card } from "antd";
import { ContractContext } from "../context/ContractContext";
import { MONSTERPEDIA } from "../data/monsterpedia";
const { Meta } = Card;

const Explore = () => {
  const contract = useContext(ContractContext);
  const [ownership, setOwnership] = useState();
  const [loading, setLoading] = useState(false);
  const [monsters, setMonsters] = useState([]);

  useEffect(() => {
    const { instance } = contract;
    // Set contract instance
    setOwnership(instance);

    const loadMonsterList = async () => {
      if (ownership) {
        let result = await ownership.methods.getAllMonsters().call();
        setMonsters(result);
      }
    };

    // Loading
    setLoading(true);
    // Retrieve all created monsters
    loadMonsterList();
    // List is loaded
    setLoading(false);
  }, [contract, ownership]);

  return (
    <div>
      <Spin tip="Loading..." spinning={loading}>
        <h1>Explore</h1>
        <Row>
          {monsters.map((mon, index) => (
            <Col span={6} style={{ padding: "16px" }} key={index}>
              <Card
                hoverable
                cover={
                  <img alt="monster" src={MONSTERPEDIA[`${mon.element}`].src} />
                }
              >
                <Meta
                  title={mon.name}
                  description={
                    <Row>
                      <Col span={4}>
                        <div
                          className={`element-tag ${MONSTERPEDIA[
                            `${mon.element}`
                          ].name.toLowerCase()}`}
                        >
                          <span role="img" aria-label="monster element">
                            {MONSTERPEDIA[`${mon.element}`].icon}
                          </span>
                        </div>
                      </Col>
                      <Col span={6}>LV: {mon.level}</Col>
                      <Col span={6}>W: {mon.win}</Col>
                      <Col span={6}>L: {mon.lose}</Col>
                    </Row>
                  }
                />
              </Card>
            </Col>
          ))}
        </Row>
      </Spin>
    </div>
  );
};

export default Explore;
