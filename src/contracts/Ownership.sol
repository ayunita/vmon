// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./IERC721.sol";
import "./Monster.sol";

contract Ownership is IERC721, Monster {
    
    mapping (uint => address) monsterApprovals;
    
    /**
     * @notice Get the balance of the owner
     * @dev Get the balance of the owner
     * @param _owner Owner address
     * @return The number of monster that owner has
     */    
    function balanceOf(address _owner) external view override returns (uint) {
        return ownerMonsterCount[_owner];
    }
    
    /**
     * @notice Get the owner of the monster
     * @dev Get the owner of the monster
     * @param _tokenId Monster ID
     */
    function ownerOf(uint _tokenId) external view override returns (address) {
        return monsterToOwner[_tokenId];
    }
    
    /**
     * @notice Helper function to transfer monster ownership
     * @dev Helper function to transfer monster ownership
     * @param _from Owner address
     * @param _to Recipient address
     * @param _tokenId Monster ID that is going to be transferred
     */    
    function _transfer(address _from, address _to, uint _tokenId) private {
        ownerMonsterCount[_to]++;
        ownerMonsterCount[msg.sender]--;
        monsterToOwner[_tokenId] = _to;
        emit Transfer(_from, _to, _tokenId);
    }
    
    /**
     * @notice Transfer monster ownership
     * @dev Transfer monster ownership
     * @param _from Beneficial/proxy owner address
     * @param _to Recipient address
     * @param _tokenId Monster ID that is going to be transferred
     */    
    function transferFrom(address _from, address _to, uint _tokenId) external override payable {
        require (monsterToOwner[_tokenId] == msg.sender || monsterApprovals[_tokenId] == msg.sender);
        _transfer(_from, _to, _tokenId);
    }

    /**
     * @notice Grant approval to proxy owner to access to the ownership of owner's monster
     * @dev Grant approval to proxy owner to access to the ownership of owner's monster
     * @param _approved Proxy owner address
     * @param _tokenId Monster ID that is going to be transferred
     */        
    function approve(address _approved, uint _tokenId) external override payable monsterOwner(_tokenId) {
        monsterApprovals[_tokenId] = _approved;
        emit Approval(msg.sender, _approved, _tokenId);
    }

}