// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./Ownable.sol";

/**
 * @title Monster
 * @author A Yunita
 * @notice Smart contract for monster creation and battle
 * @dev Owner can create, battle, and raise monster status. Operator can setup monster fee and withdraw ether.
 */
contract Monster is Ownable {    
    struct Mon {
        string name;        // Name
        uint8 element;      // Element: 0 Air, 1 Water, 2 Fire, 3 Earth
        uint8 level;        // Level, maximum is 100
        uint16 points;      // Points to increase hp/atk/def
        uint16 hp;          // Health
        uint16 atk;         // Attack
        uint16 def;         // Defense
        uint16 win;         // Win count
        uint16 lose;        // Lose count
        uint battleTime;    // Minimum timestamp for next battle
        // string imgHash;  // Image hash 
        // uint price;      // Price
        // bool forSale;    // Whether monster is for sale
    }
    
    Mon[] public monsters;                               // List of created monsters
    mapping(uint => address) public monsterToOwner;      // Map monster address with its owner address
    mapping(address => uint) ownerMonsterCount;          // Map owner address with monster count
    mapping(string => bool) private _monsterExists;      // List to track whether monster's name already exists
    uint cooldownTime = 1 hours;                         // Minimum length of time that the monster needs to wait for next battle
    uint8 battleRound = 5;                               // How many times monster can deal damage in battle
    uint8 pointsLevel = 3;                               // Receive points after level up
    uint private _newMonsterFee = 0.01 ether;
    
    event Create(string _name, uint8 _element, uint _id);
    event Battle(string _name, string _opponent, uint16 _dmg, bool _win);
    event LevelUp(string _name, uint8 _level);
    
    /**
     * @notice Set price to create new monster, default it 0.01 ether
     * @dev Set price to create new monster, default it 0.01 ether
     * @param _amount New fee
     */
    function setNewMonsterFee(uint _amount) external onlyOwner {
        _newMonsterFee = _amount;
    }
  
    /**
     * @notice Create new monster
     * @dev To create new monster, owner must send ether first. Monster is an NFT; hence, name must be unique.
     * @param _name Monster name
     * @param _element Monster element: 0 Air, 1 Water, 2 Fire, 3 Earth
     */
    function create(string memory _name, uint8 _element) external payable {
        require(msg.value == _newMonsterFee, "MONSTER: Not enough ether!");
        require(!_monsterExists[_name], "MONSTER: Name already exists");
        // Default new monster stat: HP = 10, atk = 2, def = 2
        monsters.push(Mon(_name, _element, 1, 0, 10, 2, 2, 0, 0, block.timestamp));
        // Get Monster ID
        uint _id = monsters.length - 1;
        // Track monster's name
        _monsterExists[_name] = true;
        // Assign owner to monster
        monsterToOwner[_id] = msg.sender;
        // Update owner's monster count
        ownerMonsterCount[msg.sender]++;
        emit Create(_name, _element, _id);                                  
    }
    
    // Check whether the monster ID belongs to the address
    modifier monsterOwner(uint _monsterId) {
        require(monsterToOwner[_monsterId] == msg.sender, "MONSTER: Unauthorized");
        _;
    }
    
    /**
     * @notice Fight with other monsters
     * @dev The damage calculation is based on formula: dmg=(atk-def)*constant. The constant varies, it depends on the element weakness (Element 0 > 1 > 2 > 3 > 0). If monster wins, it will level up if it meets the condition.
     * @param _monsterId Owner's monster ID
     * @param _opponentId Opponent's monster ID
     */
    function battle(uint _monsterId, uint _opponentId) external monsterOwner(_monsterId) {
        // Battle can only happen between owner's monster and other owner's monsters
        Mon storage monster = monsters[_monsterId];
        require(monster.battleTime <= block.timestamp, "MONSTER: Not ready for next battle");
        require(monsterToOwner[_opponentId] != msg.sender, "MONSTER: You cannot battle your own monster!");
        Mon storage opponent = monsters[_opponentId];
        // Set constant value by element weakness
        uint8 c = 2;
        // Element 0 > 1 > 2 > 3 > 0
        if ((monster.element == 0 && opponent.element == 1) || (monster.element == 1 && opponent.element == 2) ||  (monster.element == 2 && opponent.element == 3) || (monster.element == 3 && opponent.element == 0)) {
            c = 3;
        } else if ((monster.element == 0 && opponent.element == 3) || (monster.element == 3 && opponent.element == 2) || (monster.element == 2 && opponent.element == 1) || (monster.element == 1 && opponent.element == 0)) {
            c = 1;
        }
        // Calculate damage with formula dmg=(atk-def)*constant
        uint16 dmg = 1 * c * battleRound; // Avoid underflow
        if (monster.atk > opponent.def) {
            dmg = (monster.atk - opponent.def) * c * battleRound;
        }
        
        bool win = false;
        // Update winning and losing count
        if (dmg >= opponent.hp) {
            win = true;
            monster.win++;
            opponent.lose++;
            // Level up my monster if meet the condition
            _levelUp(monster);
        } else {
            monster.lose++;
            opponent.win++;
        }
        // Trigger cooldown
        _triggerCooldown(monster);
        
        emit Battle(monster.name, opponent.name, dmg, win);
    }
    
    /**
     * @notice Raise monster's level
     * @dev Increase monster's level by 1 iff win = current level**2. E.g. to level 2, needs total win of 1; to level 100, needs total win of 9801. Everytime monster levels up, it gets 3 points to increase stat.
     * @param _monster Monster struct
     */
    function _levelUp(Mon storage _monster) private {
        require(_monster.level < 100, "MONSTER: already at max level");
        if (_monster.win == _monster.level**2) {
            _monster.level++;                   // Level up
            _monster.points += pointsLevel;     // Get points for stat
            emit LevelUp(_monster.name, _monster.level);
        }
    }
    
    /**
     * @notice Increase monster's attack by 1
     * @dev Use 1 point to +1 attack
     * @param _monsterId Monster ID
     */
    function raiseAttack(uint _monsterId) external monsterOwner(_monsterId) {
        Mon storage monster = monsters[_monsterId];
        require(monster.points > 0, "MONSTER: Not enough points!");
        monster.atk++;
        monster.points--;
    }

    /**
     * @notice Increase monster's defense by 1
     * @dev Use 1 point to +1 defense
     * @param _monsterId Monster ID
     */    
    function raiseDefense(uint _monsterId) external monsterOwner(_monsterId) {
        Mon storage monster = monsters[_monsterId];
        require(monster.points > 0, "MONSTER: Not enough points!");
        monster.def++;
        monster.points--;
    }

    /**
     * @notice Increase monster's health by 10
     * @dev Use 1 point to +10 hp
     * @param _monsterId Monster ID
     */
    function raiseHealth(uint _monsterId) external monsterOwner(_monsterId) {
        Mon storage monster = monsters[_monsterId];
        require(monster.points > 0, "MONSTER: Not enough points!");
        monster.hp += 10;
        monster.points--;
    }    
    
    /**
     * @notice Trigger cooldown after battle. Monster can battle every 1 hour
     * @param _monster Monster struct
     */
    function _triggerCooldown(Mon storage _monster) private {
        _monster.battleTime = block.timestamp + cooldownTime;
    }
    
    /**
     * @notice Get all monsters that have been created
     * @return List of monsters
     */
    function getAllMonsters() external view returns(Mon[] memory) {
        return monsters;
    }

    /**
     * @notice Get all monster IDs the owner has
     * @param _owner Owner address
     * @return List of monster IDs
     */
    function getMonsterIdsByOwner(address _owner) external view returns(uint[] memory) {
        uint[] memory result = new uint[](ownerMonsterCount[_owner]);
        uint counter = 0;
        for (uint i = 0; i < monsters.length; i++) {
            if (monsterToOwner[i] == _owner) {
                result[counter] = i;
                counter++;
            }
        }
        return result;
    }

    /**
     * @notice Operator can withdraw ether from this contract
     */
    function withdraw() external onlyOwner {
        address _owner = owner();
        payable(_owner).transfer(address(this).balance);
    }
}
