import React, { useState, createContext } from "react";

export const ContractContext = createContext();
export const ContractActionsContext = createContext();

const ContractProvider = (props) => {
  const [contract, setContract] = useState({
    web3: null,
    account: null,
    instance: null,
  });

  return (
    <ContractContext.Provider value={contract}>
      <ContractActionsContext.Provider value={setContract}>
        {props.children}
      </ContractActionsContext.Provider>
    </ContractContext.Provider>
  );
};

export default ContractProvider;
